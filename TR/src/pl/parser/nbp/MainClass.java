package pl.parser.nbp;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MainClass {

    private static final String            XML_NAME      = "%xmlName%";
    private static final String            YEAR_PATTERN  = "%YYYY%";
    private final static DateTimeFormatter DTF           = DateTimeFormat.forPattern("yyyy-MM-dd");
    private final static DateTimeFormatter DTF_XML       = DateTimeFormat.forPattern("yyyyMMdd");
    private static final String            DIR_YEAR_LIST = "http://www.nbp.pl/kursy/xml/dir%YYYY%.txt";
    private static final String            XML_URL       = "http://www.nbp.pl/kursy/xml/" + XML_NAME + ".xml";

    public static void main(String[] args) throws IOException, ParseException {

        DateTime startDate = DTF_XML.parseDateTime("20130128");// DateTime.now().minusDays(5);
        // DTF.parseDateTime(args[1]);
        DateTime endDate = DTF_XML.parseDateTime("20130131");// DateTime.now();
        // DTF.parseDateTime(args[2]);

        String currency = "EUR";// args[0].toString();

        List<String> allUrlsForYear = getUrlsForYear(startDate, endDate);
        // extract data from xmls with obj
        List<Double> values = getValuesFromXMLs(startDate, endDate, currency, allUrlsForYear);

        DecimalFormat formatter = new DecimalFormat("#0.0000");

        Statistics s = new Statistics(values);

        System.out.println(formatter.format(s.getMean()));
        System.out.println(formatter.format(s.getStdDev()));
        
        return;

    }

    private static List<Double> getValuesFromXMLs(DateTime startDate, DateTime endDate, String currency,
            List<String> allUrlsForYear) throws IOException, ParseException {
        List<Double> values = new ArrayList<>();
        Document doc;
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        for (String xml : allUrlsForYear) {
            DateTime xmlDate = DTF_XML.parseDateTime("20" + xml.substring(xml.length() - 6, xml.length()));
            if (xml.matches("c.*?") && xmlDate.getMillis() >= startDate.getMillis()
                    && xmlDate.getMillis() <= endDate.getMillis()) {
                String urlXml = XML_URL.replace(XML_NAME, xml);
                doc = Jsoup.connect(urlXml).get();
                doc = Jsoup.parse(doc.toString());
                Elements positions = doc.getElementsByTag("pozycja");
                for (Element el : positions) {
                    if (el.toString().contains(currency)) {
                        String value = el.getElementsByTag("kurs_kupna").text();
                        values.add(format.parse(value).doubleValue());
                    }
                }
            }
        }
        return values;
    }

    private static List<String> getUrlsForYear(DateTime startDate, DateTime endDate) throws IOException {
        Document doc;
        List<String> allUrlsForYear = new ArrayList<>();
        for (DateTime date = startDate; date.getYear() <= endDate.getYear(); date = date.plusYears(1)) {
            String year = Integer.toString(date.getYear());
            String yearCurrent = DateTime.now().getYear() + "";
            if (year.equals(yearCurrent)) {
                year = "";
            }
            String url = DIR_YEAR_LIST.replace(YEAR_PATTERN, year);
            doc = Jsoup.connect(url).get();
            allUrlsForYear.addAll(Arrays.asList(doc.body().getAllElements().html().split(" ")));
        }
        return allUrlsForYear;
    }

}
