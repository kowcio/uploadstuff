package tr;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.annotations.Test;

public class TaskTwoPowerCalculations {

	private static final int	TIMEOUT_10_S	    = 10000;
	private static final String	ACTUAL_CELL_CLASS	= "txtrVERIF";
	private static final String	FORECAST_CELL_CLASS	= "txtrPREV";
	private static final String	DATA_URL	        = "http://www.mercado.ren.pt/EN/Electr/MarketInfo/Interconnections/CapForecast/Pages/Daily.aspx";
	private static final String	NO_BREAK_SPACE_CODE	= "\u00a0";

	public static void main(String[] args) {
		Document doc = null;

		int retry = 0;
		while (retry < 3) {
			try {
				doc = Jsoup.connect(DATA_URL).timeout(TIMEOUT_10_S).get();
				break;
			} catch (IOException e) {
				retry++;
				System.out.println("Propably connection problem, trying again.");
			}
		}

		Elements mainGridAll = doc.select(".gridALL");
		Elements dates = mainGridAll.select(".tabHEADER").select("th");
		dates.remove(0);

		List<String> parsedDates = getColumnsHeadersDates(dates);

		int columns = parsedDates.size();

		List<Element> rows = mainGridAll.select("tr[class*=PAR]");

		Integer[] actualSum = new Integer[columns];
		Integer[] forecastSum = new Integer[columns];
		Arrays.fill(actualSum, 0);
		Arrays.fill(forecastSum, 0);

		for (int rowNo = 0; rowNo < rows.size(); rowNo++) {
			Element row = rows.get(rowNo);

			for (int columnNo = 1; columnNo < columns; columnNo++) {
				Element cell = row.select("td").get(columnNo);

				if (cell.hasClass(FORECAST_CELL_CLASS)) {
					forecastSum[columnNo] += Integer.parseInt(cell.text());

				} else if (cell.hasClass(ACTUAL_CELL_CLASS)) {
					actualSum[columnNo] += Integer.parseInt(cell.text());
				} else {
					System.out.println("No class found.");
				}
			}
		}

		displayResults(parsedDates, columns, actualSum, forecastSum);

	}

	private static void displayResults(List<String> parsedDates, int columns, Integer[] actualSum, Integer[] forecastSum) {
		for (int sumToDisplay = 1; sumToDisplay < columns; sumToDisplay++) {
			System.out.println("Actual:" + parsedDates.get(sumToDisplay) + ":" + actualSum[sumToDisplay]);
			System.out.println("Forecast:" + parsedDates.get(sumToDisplay) + ":" + forecastSum[sumToDisplay]);
		}
	}

	private static List<String> getColumnsHeadersDates(Elements dates) {
		return dates.stream()
		        .map(date -> parseDateFromTableHeaderToString(date.text() + "-" + LocalDate.now().getYear()))
		        .collect(toList());
	}

	public static String parseDateFromTableHeaderToString(String date) {
		String localDate = date.replaceAll(NO_BREAK_SPACE_CODE, "-");
		String datePattern = "dd-MMM-yyyy";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern, Locale.US);
		LocalDate dateTime = LocalDate.parse(localDate, formatter);
		return dateTime.toString();
	}

	@Test
	public void parseDateTest() {
		String str = "10-Apr-2015";
		String datePattern = "dd-MMM-yyyy";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern, Locale.UK);
		LocalDate date = LocalDate.parse(str, formatter);
		System.out.println("date = " + date);
	}

}
