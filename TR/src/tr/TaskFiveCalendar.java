package tr;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class TaskFiveCalendar {

	public static void main(String[] args) {

		String start = "01/01/2015";
		String end = "12/09/2015";

		LocalDate day = parseDataFromFormat(start);
		LocalDate endDate = parseDataFromFormat(end);
		
		List<LocalDate> weekdays = new ArrayList<>();

		while (day.isBefore(endDate)) {
			if (isDayWeekend(day)) {
				weekdays.add(day);
			}
			day = day.plusDays(1);
		}

		weekdays.forEach(c -> System.out.println(c + " - " + c.getDayOfWeek()));

	}

	private static boolean isDayWeekend(LocalDate day) {
		return day.getDayOfWeek().compareTo(DayOfWeek.SATURDAY) < 0
		        && day.getDayOfWeek().compareTo(DayOfWeek.SUNDAY) < 0;
	}

	public static LocalDate parseDataFromFormat(String date) {
		String datePattern = "dd/MM/yyyy";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern, Locale.US);
		return LocalDate.parse(date, formatter);
	}
}
