package tr;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

public class TaskThreeHurricains {

	private static final String	DATA_FILE	  = "hurricanesData.txt";
	private static final String	YEAR_TO_CHECK	= "2009";

	public static void main(String[] args) throws IOException {

		URL url = TaskThreeHurricains.class.getResource(DATA_FILE);

		Stream<String> lines = Files.lines(Paths.get(url.getPath().substring(1)));

		Iterator<String> i = lines.iterator();
		while (i.hasNext()) {
			List<Integer> maxWindFromSampling = new ArrayList<>();

			String row = (String) i.next();

			if (row.contains(YEAR_TO_CHECK)) {

				List<String> hurricaneMainData = Arrays.asList(StringUtils.deleteWhitespace(row).split(","));
				int rowsToCheckSpeed = Integer.parseInt(hurricaneMainData.get(2));

				for (int j = 0; j < rowsToCheckSpeed; j++) {
					List<String> hurricaneKnotData = Arrays.asList(StringUtils.deleteWhitespace((String) i.next())
					        .split(","));
					maxWindFromSampling.add(Integer.parseInt(hurricaneKnotData.get(6)));
				}
				System.out.println(hurricaneMainData.get(1) + " = " + Collections.max(maxWindFromSampling));

			}

		}
		lines.close();

	}

}
