package tr;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

public class TaskFourMartians {

	public static void main(String[] args) {

		List<String> customWords = Arrays.asList("Tango", "Beta", "Charlie", "Foxtrot");
		List<String> reversedCustomWordsForChecking = Arrays.asList("ognaT", "ateB", "eilrahC", "tortxoF");

		for (int i = 0; i < customWords.size(); i++) {

			String martianWord = new StringBuilder(customWords.get(i)).reverse().toString();
			System.out.println(martianWord);
			Assert.assertTrue(martianWord.equals(reversedCustomWordsForChecking.get(i)));

		}

	}

}
