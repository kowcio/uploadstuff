package tr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class XLS {

    private static final int FIRST_CELL = 2;
    private static final int FIRST_ROW  = 20;

    static DateTimeZone      CET        = DateTimeZone.forID("CET");
    static DateTimeZone      UTC        = DateTimeZone.forID("UTC");
    static DateTimeFormatter dtf        = DateTimeFormat.forPattern("dd/MM/yyyy").withZone(CET);
    static String            dateString = ("dd.MM HH:mm:ss ZZ zzz");

    public static void main(String[] args) throws IOException {

        FileInputStream file = new FileInputStream(new File(
                "C:\\Users\\Kowcio\\Desktop\\Historique_consommation_INST_2014.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);

        String date = sheet.getRow(20).getCell(0).toString();
        DateTime startDate = dtf.parseDateTime(date).minusMinutes(30);
        System.out.println("Start date " + startDate);
        int day, pointsInMonth = 0;
        int point = 1;
        // iterate rows
        for (int rowNo = FIRST_ROW; rowNo < sheet.getLastRowNum(); rowNo++) {

            Row row = sheet.getRow(rowNo);

            if (row == null) {
//                System.out.println("Skipping empty row");
                continue;
            }
            pointsInMonth = 1;
            // iterate cells
            for (int cellNo = FIRST_CELL; cellNo < row.getLastCellNum(); cellNo++, pointsInMonth++, point++) {
                Cell cell = row.getCell(cellNo);
                

                boolean toCEST = isDayWithChangeToDaylightSavings(startDate);
                boolean fromCEST = isDayWithChangeFromDaylightSavings(startDate.withZone(UTC));
                if ((pointsInMonth==5 || pointsInMonth==6) && toCEST){
                    System.out.println("SKIPPED " + pointsInMonth + " row = " + row.getRowNum());
                    continue;
                }
                if ((pointsInMonth==5 || pointsInMonth==6) && fromCEST){
                    System.out.println("ADDED " + pointsInMonth + " row = " + row.getRowNum());
                    continue;
                }
                
                startDate = startDate.plusMinutes(30);
                
                DateTime start = new DateTime(startDate.getYear(), 3, 29, 0, 0, 0, 0).minusMillis(1);
                DateTime end = new DateTime(startDate.getYear(), 3, 31, 6, 0, 0, 0).minusMillis(1);
                DateTime startW = new DateTime(startDate.getYear(), 10, 25, 23, 0, 0, 0).minusMillis(1);
                DateTime endW = new DateTime(startDate.getYear(), 10, 28, 6, 0, 0, 0).minusMillis(1);

                if (startDate.isAfter(start) && startDate.isBefore(end)) {
                    System.out.println("CEST Date = " + startDate.toString(dateString) +"-" + rowNo + " Value = "
                            + cell.toString() + " points = " + pointsInMonth + " toCEST + " + toCEST);
                }
                if (startDate.isAfter(startW) && startDate.isBefore(endW)) {
                    System.out.println(" CET Date = " + startDate.toString(dateString) +"-" + rowNo + " Value = "
                            + cell.toString() + " points = " + pointsInMonth + " fromCEST " + fromCEST);
                }
                
                
            }

        }

    }
    
    public static boolean isDayWithChangeToDaylightSavings(DateTime date) {
        date = date.withMillisOfDay(0);
        return date.plusHours(23).equals(date.plusDays(1));
    }

    public static boolean isDayWithChangeFromDaylightSavings(DateTime date) {
        date = date.withMillisOfDay(0);
        return date.plusHours(25).equals(date.plusDays(1));
    }
    
    
}
